Imageinfo Cache
===============

-- ABOUT

Imageinfo Cache solves a few specific problems with Drupal images and scaling:

Problem A: Your setup went from 1 server to multiple servers. 
theme_imagecache()/theme_imagefield_image() is taking a lot longer due to the files directory 
being a NFS mount (etc...).
Solution: Caching so PHP doesn't hit the disk to get the image info.

Problem B: Waiting for imagecache to generate all the images after you press node save.
Solution: Generate imagecache images right after the image is uploaded.

Problem C: You are dealing with a metric ton of images, and imagecache generation drags down
your server performance (both in realtime, and when generated in bulk.)
Solution: Queue the image jobs and work them off, either on the same server or from N additional
servers. 

Explanation:

1) Imageinfo Cache adds a caching layer right before theme_imagecache()/theme_imagefield_image() 
is called; thus future requests do not access the disk when requesting image_get_info() which calls 
getimagesize() and filesize(). This saves disk hits, and if your files directory is shared, you 
might see some big improvements in performance as a result.

2) Imageinfo Cache will grab the image right after it has been uploaded from the CCK widget and 
will selectively generate some or all imagecache presets. It will also run the file through the 
2 theme functions mentioned above so the cache is primed. This happens in the background, 
not taking any resources from the server.

3) Imageinfo Cache in Queue mode will send all requests to Queue API job queues. These queues
can be configured to only work off and generate presets when the system load is below a certain
user-defined figure (IE, 'uptime' load average below 3.00.) 

-- ASSUMPTIONS

For either HTTP or Queue mode, offloading images is going to require a
separate server. The setup that has been tested is a central database server
with additional 


-- QUEUE MODE DETAILS

The implementation approach uses job queueing and locking. Specifically,
realtime requests for imagefield images that do not have a generated preset
are sent to a Drupal Queue API queue. 

Speed of queueing and working is important, and by default the Drupal Queue
API uses the database table 'queue' to store jobs. This does not provide a
"realtime" queue and would otherwise require polling to obtain jobs to work
off. To remedy this, it is recommended to use the Beanstalkd module:
http://drupal.org/project/beanstalkd. This provides a Drupal interface for the
Beanstalkd daemon. Beanstalkd is a simple job queue, similar in approach to
what Memcache is for caching. The beanstalkd Drupal module enables selective
moving of Queue API queues to beanstalkd. Workers can maintain a persistent
connection to the beanstalkd queue to process jobs immediately upon receiving
them, assuming the load average is within your defined range.

Locking is also an issue. ImageCache by default uses file locking. In this
module, that was changed to use lock.inc. By default, Drupal uses a database table
with a relatively long poll time to check on the availability of locks. In the
case of this module, a lock is set prior to sending an item to the queue and
removed upon generation of the image. This is all done while 'holding' the php
thread open so the user does not receive an empty image. Basically, for this
to work effectively locks need to be faster. The recommended solutions are to
move locking into a memcache queue (via http://drupal.org/project/memcache) so
no SQL tables are involved, and then to apply the core patch to lock.inc to
reduce the lock wait time. Both of these changes should have other positive
performance impacts on a Drupal system as locking is used throughout core and
other modules.


Required: http://drupal.org/project/drupal_queue
Recommended: http://drupal.org/project/beanstalkd + pheanstalk
Recommended: http://drupal.org/project/memcache (Replace core locking)
Recommended: http://drupal.org/node/802856 #30 (Reduce lock wait time)
Background info on db locking for ImageCache: http://drupal.org/node/1243258

-- INSTALLATION

1) Install prerequisite modules, drupal_queue, httprl.

2) Determine if you wish to use HTTP mode or Queue mode, and follow along
below:

-- HTTP MODE INSTALLATION

2) < TODO >


-- QUEUE MODE INSTALLATION

3) If you have memcache, configure it for a lock.inc replacement. Generally
this means installing memcache, the Drupal memcache module, and adding something 
like this to your settings.php:
$conf['cache_inc'] = '/mysite/sites/all/modules/memcache/memcache.inc';
$conf['lock_inc'] = '/mysite/sites/all/modules/memcache/memcache-lock.inc';
$conf['memcache_servers'] = array(
  '127.0.0.1:11211' => 'default',
);
$conf['memcache_bins'] = array(
  'cache'               => 'default',
);

Consult the memcache module documentation for a more thorough example.

4) Consider applying the recommended core patch to reduce lock wait time.

5) Install beanstalkd, pheanstalk, and the beanstalkd Drupal module. Configure
your settings.php to enable beanstalk support for the imageinfo_cache queues.
Example config:

/**
 * Beanstalkd - Queue API replacement for database queueing
 */
$conf['beanstalk_default_queue'] = array(
  'host' => 'localhost', // Name of the host where beanstalkd is installed.
  'port' => '11300', // Port which beanstalkd is listening to.
  'fork' => FALSE, // Used in runqueue.sh to know if it should run the job in another process.
  'reserve_timeout' => 0, // How long you should wait when reserving a job.
  'ttr' => 60, // Seconds a job can be reserved for
  'release_delay' => 0, // Seconds to delay a job
  'priority' => 1024, // Sets the priority of the job
);
$conf['queue_class_imageinfo_cache_queue_vip'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_imageinfo_cache_queue_vip'] = array(
  'host' => 'localhost',
  'port' => '11300',
  'fork' => FALSE,
  'reserve_timeout' => 0,
  'ttr' => 180,
  'release_delay' => 0,
  'priority' => 512,
);
$conf['queue_class_imageinfo_cache_queue_normal'] = 'BeanstalkdQueue';
$conf['beanstalk_queue_imageinfo_cache_queue_normal'] = array(
  'host' => 'localhost',
  'port' => '11300',
  'fork' => FALSE,
  'reserve_timeout' => 0,
  'ttr' => 180,
  'release_delay' => 0,
  'priority' => 1024,
);

Note that the above configuration assumes that the beanstalkd process is running on the same machine
as the image generation node. If not, adjust your 'host' settings appropriately. For example, if you
have a primary server that runs your database+http process and has beanstalk installed locally, use
localhost. If you add a new webserver-only node (for image generation), the settings.php would reflect
a 'host' => '10.1.2.3', where that is the IP of the primary server.

6) Enable the imageinfo_cache module in the normal way. Clear your menu router cache
(/admin/settings/performance) via the "Clear cached data" button to make sure the changes are
picked up properly.

7) Configure the appropriate settings and mode on the imageinfo_cache configuration page in
the admin section of your site.
