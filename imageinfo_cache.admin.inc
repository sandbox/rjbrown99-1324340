<?php

/**
 * @file
 *   Configuration page for imageinfo cache module.
 */

/**
 * Page generation fucntion for admin/settings/imageinfo-cache
 */
function imageinfo_cache_admin_page() {
  $output = '';
  return $output . drupal_get_form('imageinfo_cache_admin_form');
}

/**
 * Form builder; Displays configuration page.
 */
function imageinfo_cache_admin_form($form_state) {
  $form = array();

  $form['imageinfo_cache_mode'] = array(
    '#type'           => 'radios',
    '#title'          => t('Image Generation Mode'),
    '#default_value'  => variable_get('imageinfo_cache_mode', IMAGEINFO_CACHE_MODE),
    '#description'    => t('HTTP mode uses Server->Client HTTPRL push, Queue mode uses the Queue API with Client->Server processing.'),
    '#options'        => array(
      0 => t('HTTP'),
      1 => t('Queue'),
    ),
  );
  $form['imageinfo_cache_imagecache_pregenerate'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Use imagecache pre-generation'),
    '#default_value'  => variable_get('imageinfo_cache_imagecache_pregenerate', IMAGEINFO_CACHE_IMAGECACHE_PREGENERATE),
    '#description'    => t('What presets are pre-generated can be selected on the CCK filefield widget settings on the admin/content/node-type/%/fields/% page.'),
  );
  $form['imageinfo_cache_fields'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Imageinfo Cache CCK Settings'),
    '#collapsible'    => TRUE,
    '#collapsed'      => TRUE,
  );

  $links = array();
  foreach (content_types() as $node_type => $info) {
    foreach ($info['fields'] as $filed_name => $data) {
      if ($data['type'] == 'filefield') {
        $links[] = l($info['name'] . ' - ' . $data['field_name'], 'admin/content/node-type/' . str_replace('_', '-', $node_type) . '/fields/' . $filed_name);
      }
    }
  }
  $form['imageinfo_cache_fields']['list'] = array(
    '#type'           => 'markup',
    '#value'          => implode('<br />', $links),
  );

  $form['imageinfo_cache_http'] = array(
    '#type'             => 'fieldset',
    '#title'            => t('HTTP Configuration'),
  );
  $form['imageinfo_cache_http']['imageinfo_cache_httprl_mode'] = array(
    '#type'           => 'radios',
    '#title'          => t('HTTP Mode'),
    '#default_value'  => variable_get('imageinfo_cache_httprl_mode', IMAGEINFO_CACHE_HTTPRL_MODE),
    '#description'    => t('Blocking mode will wait for an acknowledgement from the server that the http request was recieved. Non Blocking mode will not wait for any acknowledgement that the request was recieved. Non Blocking mode is faster but does not guarantee that the work will be done. Blocking mode is slower and guarantees that the work will be done.'),
    '#options' => array(
      0 => t('Non Blocking'),
      1 => t('Blocking'),
    ),
  );
  $form['imageinfo_cache_http']['imageinfo_cache_async_max'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Max number of files to send to each async worker'),
    '#default_value'  => variable_get('imageinfo_cache_async_max', IMAGEINFO_CACHE_ASYNC_MAX),
//     '#description'    => t(''),
  );
  $form['imageinfo_cache_http']['imageinfo_cache_url_key'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Shared authentication key'),
    '#default_value'  => variable_get('imageinfo_cache_url_key', md5(drupal_get_private_key())),
    '#description'    => t('Use a 32 character md5 hash. This must match on all servers.'),
  );
  $form['imageinfo_cache_http']['imageinfo_cache_server_addr'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Server/IP Address to send all image generation requests to'),
    '#default_value'  => variable_get('imageinfo_cache_server_addr', FALSE),
    '#description'    => t('If left blank it will use the settings found in the <a href="!link">HTTPRL module</a>.', array('!link' => url('admin/settings/httprl'))),
  );

  $form['imageinfo_cache_queue'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Queue API Configuration'),
  );
  $form['imageinfo_cache_queue']['imageinfo_cache_process_threshold'] = array(
    '#type'           => 'textfield',
    '#title'          => t('System load threshold for image generation'),
    '#default_value'  => variable_get('imageinfo_cache_process_threshold', 3),
    '#description'    => t('The maximum system load average, above which images will not be generated. Run the uptime command to see current load.'),
  );
  $form['imageinfo_cache_queue']['imageinfo_cache_process_lifetime'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Queue process lifetime'),
    '#default_value'  => variable_get('imageinfo_cache_process_lifetime', 3600),
    '#description'    => t('The maximum lifetime of a queue run. PHP is single threaded, so this controls the maximum time a single PHP thread will process queue workers.'),
  );
  $form['imageinfo_cache_queue']['imageinfo_cache_delete_on_exception'] = array(
    '#type'           => 'radios',
    '#title'          => t('Delete on Exception'),
    '#default_value'  => variable_get('imageinfo_cache_delete_on_exception', TRUE),
    '#description'    => t('Deletes the job from the queue if there was an error claiming it or generating the image.'),
    '#options' => array(
      0 => t('Do not delete'),
      1 => t('Delete'),
    ),
  );

  $period = drupal_map_assoc(array(1800, 2700, 3600, 10800, 21600, 32400, 43200, 64800, 86400, 2*86400, 3*86400, 4*86400, 5*86400, 6*86400, 604800, ), 'format_interval');
  $form['imageinfo_cache_lifetime'] = array(
    '#type'           => 'select',
    '#title'          => t('Theme cache lifetime'),
    '#default_value'  => variable_get('imageinfo_cache_lifetime', IMAGEINFO_CACHE_LIFETIME),
    '#options'        => $period,
  );

  return system_settings_form($form);
}

/**
 * validate imageinfo_cache_admin_form submissions.
 */
function imageinfo_cache_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  // Check that imageinfo_cache_async_max is a positive interger.
  if (   empty($values['imageinfo_cache_async_max'])
      || !is_numeric($values['imageinfo_cache_async_max'])
      || $values['imageinfo_cache_async_max'] < 1
        ) {
    form_set_error('imageinfo_cache_async_max', t('Number must be a positive interger.'));
  }

  // If the IP field is not blank, check that its a valid address.
  if (!empty($values['imageinfo_cache_server_addr'])) {
    // Not an IP address
    if (ip2long($values['imageinfo_cache_server_addr']) === FALSE) {
      // Not a valid hostname
      if (ip2long(gethostbyname($values['imageinfo_cache_server_addr'])) === FALSE) {
        form_set_error('imageinfo_cache_server_addr', t('Must be a valid IP address OR server name.') . gethostbyname($values['imageinfo_cache_server_addr']));
      }
    }
  }

  if (   empty($values['imageinfo_cache_url_key'])
      || !preg_match('/^[a-f0-9]{32}$/', $values['imageinfo_cache_url_key'])
        ) {
    form_set_error('imageinfo_cache_url_key', t('Value must be a valid md5 hash. Recommended md5 hash on this server: %value', array('%value' => md5(drupal_get_private_key()))));
  }

  // Do no set certian variables.
  unset($form_state['values']['list']);
}

/**
 * Called from imageinfo_cache_form_alter().
 */
function imageinfo_cache_cck_widget_form(&$form, $form_state, $form_id) {
  $widget_settings = variable_get('imageinfo_cache_cck_widget_' . $form['#field']['type_name'] . '_' . $form['#field']['field_name'], IMAGEINFO_CACHE_CCK_WIDGET);
  $all = FALSE;
  if (!is_array($widget_settings) && $widget_settings == TRUE) {
    $all = TRUE;
  }

  // Add in settings form.
  $form['widget']['imageinfo_cache'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Imageinfo Cache'),
    '#collapsible'    => 1,
    '#collapsed'      => 1,
    '#weight'         => 20,
  );
  $form['widget']['imageinfo_cache']['imageinfo_cache_settings_all'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Select all imagecache presets to pregenerate when an image is uploaded'),
    '#default_value'  => $all,
    '#description'    => t('If not selected, settings will apear below allowing one to control what presets will be pregenerated.'),
    '#disabled'       => variable_get('imageinfo_cache_imagecache_pregenerate', IMAGEINFO_CACHE_IMAGECACHE_PREGENERATE) ? FALSE : TRUE,
  );
  foreach (imagecache_presets() as $preset) {
    $presets[$preset['presetid']] = $preset['presetname'];
  }
  $form['widget']['imageinfo_cache']['imageinfo_cache_settings'] = array(
    '#type'           => 'checkboxes',
    '#title'          => t('Select the imagecache presets to pregenerate when an image is uploaded'),
    '#options'        => $presets,
    '#default_value'  => $all ? array_flip($presets) : $widget_settings,
    '#prefix'         => '<div id="imageinfo-cache-settings-widget">',
    '#suffix'         => '</div>',
  );

  // Add in submit handler.
  if (!in_array('imageinfo_cache_cck_widget_form_submit', $form['#submit'])) {
    $form['#submit'][] = 'imageinfo_cache_cck_widget_form_submit';
  }

  $widget_vis = "
$(imageinfo_cache_update_widget_visibility);
$(function(){ $('#edit-imageinfo-cache-settings-all').change(function (){imageinfo_cache_update_widget_visibility();})});
function imageinfo_cache_update_widget_visibility() {
  if ($('#edit-imageinfo-cache-settings-all:checked').val() !== undefined) {
    $('#imageinfo-cache-settings-widget').hide();
  }
  else {
    $('#imageinfo-cache-settings-widget').show();
  }
}";
  drupal_add_js($widget_vis, 'inline');
}

/**
 * Implements hook_form_submit().
 */
function imageinfo_cache_cck_widget_form_submit($form, &$form_state) {
  if (empty($form_state['values']['imageinfo_cache_settings'])) {
    return;
  }

  if ($form_state['values']['imageinfo_cache_settings_all']) {
    variable_set('imageinfo_cache_cck_widget_' . $form['#field']['type_name'] . '_' . $form['#field']['field_name'], $form_state['values']['imageinfo_cache_settings_all']);
  }
  else {
    variable_set('imageinfo_cache_cck_widget_' . $form['#field']['type_name'] . '_' . $form['#field']['field_name'], $form_state['values']['imageinfo_cache_settings']);
  }
  unset($form_state['values']['imageinfo_cache_settings']);
  unset($form_state['values']['imageinfo_cache_settings_all']);
}
