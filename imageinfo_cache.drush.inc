<?php

/**
 * @file
 *   Drush commands for Queue support
 */

/**
 * Implements hook_drush_help().
 */
function imageinfo_cache_drush_help($section) {
  switch ($section) {
    case 'drush:imageinfo-cache-queue':
    return dt('Run imageinfo_cache Drupal queues. Allows for processing queues that block indefinitely waiting for new jobs without holding up other queues. Optional argument of the queue name if you only want to run one, imageinfo_cache_queue_vip or imageinfo_cache_queue_normal');
  }
}

/**
 * Implements hook_drush_command().
 */
function imageinfo_cache_drush_command() {
  $items = array();

  $items['imageinfo-cache-queue'] = array(
    'callback' => 'imageinfo_cache_process_queues',
    'description' => dt('Run the imageinfo_cache Drupal queues.'),
    'arguments' => array(
      'queue_to_process' => dt('Optional: The name of the queue to process.'),
    ),
  );
  return $items;
}
